import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()

    result = []

    df['Title'] = df['Name'].str.extract('([A-Za-z]+)\.', expand=False)
    
    titles = ['Mr', 'Mrs', 'Miss']
    
    for title in titles:
        title_mask = df['Title'] == title

        median_age = int(df.loc[title_mask, 'Age'].median())
        missing_values = df.loc[title_mask & df['Age'].isnull(), 'Age'].shape[0]
        
        df.loc[title_mask & df['Age'].isnull(), 'Age'] = median_age
        
        result.append((title + '.', missing_values, median_age))
    
    return result
